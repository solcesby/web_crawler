package service;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static utils.LinkUtil.cutLink;
import static utils.LinkUtil.isProperLink;

public class WebCrawler {
    /**
     *
     * @param url
     * @param terms
     * @return
     */
    public Map<String, Long> countMentions(String url, String... terms) {
        Map<String, Long> mentionsMap = prepareMentionsMap(terms);
        try {
            Document doc = Jsoup.connect(url).get();
            String[] splittedBody = doc.body().text()
                    .replaceAll("[^а-яёА-ЯЁa-zA-Z0-9_]", " ")
                    .split(" +");
            for (String word : splittedBody) {
                for (String term : terms) {
                    if (word.equalsIgnoreCase(term)) {
                        Long oldCount = mentionsMap.get(term);
                        mentionsMap.put(term, oldCount + 1);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mentionsMap;
    }

    /**
     *
     * @param url
     * @param linkDepth
     * @param maxVisitedPages
     * @return
     */
    public Set<String> getAllLinks(String url, Long linkDepth, Long maxVisitedPages) {
        Set<String> linksToCheck = new HashSet<>();
        Set<String> foundLinks = new HashSet<>();
        linksToCheck.add(url);
        foundLinks.add(url);
        for (long i = 0L; i < linkDepth; i++) {
            if (foundLinks.size() == maxVisitedPages) {
                return foundLinks;
            }

            Set<String> newLinks = new HashSet<>();
            for (String link : linksToCheck) {
                if (isProperLink(link)) {
                    newLinks.addAll(getLinks(link));
                }
            }
            linksToCheck.addAll(newLinks);
            linksToCheck.removeAll(foundLinks);
            newLinks.forEach(link -> {
                if (foundLinks.size() < maxVisitedPages) {
                    foundLinks.add(link);
                } else {
                    return;
                }
            });
        }
        return foundLinks;
    }

    /**
     *
     * @param url
     * @return
     */
    private Set<String> getLinks(String url) {
        Set<String> properLinksSet = new HashSet<>();
        try {
            Document doc = Jsoup.connect(url).get();
            Set<Element> links = new HashSet<>(doc.select("a[href]"));
            for (Element link : links) {
                String properLink = cutLink(link.attr("abs:href"));
                if (isProperLink(properLink)) {
                    properLinksSet.add(properLink);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properLinksSet;
    }

    /**
     *
     * @param terms
     * @return
     */
    private Map<String, Long> prepareMentionsMap(String... terms) {
        Map<String, Long> mentionsMap = new HashMap<>();
        for (String term : terms) {
            mentionsMap.put(term, 0L);
        }
        return mentionsMap;
    }
}
