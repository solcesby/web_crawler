package service;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class CSVWriter {

    private static final String outFile = "out.csv";
    private static final String topTenOutFile = "top_ten_out.csv";
    private static final byte maxTopValue = 10;

    /**
     *
     * @param mentionsMap
     * @param url
     */
    public void write(Map<String, Long> mentionsMap, String url) {
        File file = new File(outFile);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {
            String representation = buildStringRepresentation(mentionsMap, url);
            writer.write(representation);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     */
    public void writeTopMentions() {
        File fileOut = new File(topTenOutFile);
        Pair pair = readMentionsMap();
        writeTopMentionsFile(fileOut, pair);
    }

    /**
     *
     * @param mentionsMap
     * @param url
     * @return
     */
    private String buildStringRepresentation(Map<String, Long> mentionsMap, String url) {
        StringBuilder sb = new StringBuilder();
        Long total = 0L;
        sb.append(url);
        sb.append(',');
        for (String key : mentionsMap.keySet()) {
            sb.append(mentionsMap.get(key));
            sb.append(',');
            total += mentionsMap.get(key);
        }
        sb.append(total);
        sb.append('\n');
        return sb.toString();
    }

    /**
     *
     * @return
     */
    private Pair readMentionsMap() {
        File fileIn = new File(outFile);
        Map<String, Long> mapIn = new HashMap<>();
        List<String> lines = new LinkedList<>();
        if (!fileIn.exists()) {
            return null;
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(fileIn))) {
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line);
                String[] values = line.split(",");
                mapIn.put(values[0], Long.parseLong(values[values.length - 1]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Pair(mapIn, lines);
    }

    /**
     *
     * @param fileOut
     * @param pair
     */
    private void writeTopMentionsFile(File fileOut, Pair pair) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileOut))) {
            if (pair != null) {
                Map<String, Long> mapIn = pair.map;
                List<String> lines = pair.list;
                Map<String, Long> mapInSorted = sortMentionsMap(mapIn);
                int counter = 0;
                for (String key : mapInSorted.keySet()) {
                    for (String line : lines) {
                        if (counter < maxTopValue && line.contains(key)) {
                            writer.write(line + '\n');
                            counter++;
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param map
     * @return
     */
    private Map<String, Long> sortMentionsMap(Map<String, Long> map) {
        return map.entrySet()
                .stream()
                .sorted((Map.Entry.<String, Long>comparingByValue().reversed()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    /**
     *
     */
    private static class Pair {
        public final Map<String, Long> map;
        public final List<String> list;

        /**
         *
         * @param map
         * @param list
         */
        public Pair(Map<String, Long> map, List<String> list) {
            this.map = map;
            this.list = list;
        }
    }
}
