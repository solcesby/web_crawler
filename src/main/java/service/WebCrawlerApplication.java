package service;

public class WebCrawlerApplication {

    private static final CSVWriter writer = new CSVWriter();

    /**
     * Starts the Web Crawler
     *
     * @param url of the page where to start looking for links and terms
     * @param linkDepth is how many times it can click links from start page to end page
     * @param maxVisitedPages is how many pages it can parse
     * @param terms word(-s) to look for on the page(-s)
     */
    public static void run(String url, Long linkDepth, Long maxVisitedPages, String... terms) {
        WebCrawler crawler = new WebCrawler();
        crawler.getAllLinks(url, linkDepth, maxVisitedPages)
                .forEach(link -> writer.write(crawler.countMentions(link, terms), link));
        writer.writeTopMentions();
    }

}
