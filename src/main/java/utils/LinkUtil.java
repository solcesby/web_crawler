package utils;

public class LinkUtil {
    /**
     * Returns true if link can be used by Jsoup
     * Avoiding links with ',' because this link is supposed to be written to .CSV file
     *
     * @param string to check
     * @return {@code true} if this {@code String} not {@code null}, not empty,
     *         starts with HTTP (also works for HTTPS) and doesn't contain ',', {@code false} otherwise
     */
    public static boolean isProperLink(String string) {
        return string != null && string.length() != 0 && string.startsWith("http") && !string.contains(",");
    }

    /**
     * Returns first part of the link before '#' character
     * limit 2 is used to increase performance
     *
     * @param link to trim
     * @return first part of {@code link} before '#' character
     */
    public static String cutLink(String link) {
        return link.split("#", 2)[0];
    }

}
