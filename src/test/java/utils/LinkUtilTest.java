package utils;

import org.junit.Assert;
import org.junit.Test;

import static utils.LinkUtil.cutLink;
import static utils.LinkUtil.isProperLink;

public class LinkUtilTest {

    @Test
    public void testIsProperLink_ifNull() {
        //GIVEN
        String link = null;

        //WHEN
        boolean result = isProperLink(link);

        //THEN
        Assert.assertFalse(result);
    }

    @Test
    public void testIsProperLink_ifEmpty() {
        //GIVEN
        String link = "";

        //WHEN
        boolean result = isProperLink(link);

        //THEN
        Assert.assertFalse(result);
    }

    @Test
    public void testIsProperLink_ifMailto() {
        //GIVEN
        String link = "mailto:help@lipsum.com";

        //WHEN
        boolean result = isProperLink(link);

        //THEN
        Assert.assertFalse(result);
    }

    @Test
    public void testIsProperLink_ifStartsWithHTTP() {
        //GIVEN

        String link = "http://fortune.com/2016/12/18/elon-musk-traffic-boring/";

        //WHEN
        boolean result = isProperLink(link);

        //THEN
        Assert.assertTrue(result);
    }

    @Test
    public void testIsProperLink_ifStartsWithHTTPS() {
        //GIVEN
        String link = "https://en.wikipedia.org/wiki/Elon_Musk";

        //WHEN
        boolean result = isProperLink(link);

        //THEN
        Assert.assertTrue(result);
    }

    @Test
    public void testIsProperLink_ifContainsComma() {
        //GIVEN
        String link = "https://en.wikipedia.org/wiki/Tesla,_Inc.";

        //WHEN
        boolean result = isProperLink(link);

        //THEN
        Assert.assertFalse(result);
    }

    @Test
    public void testCutLink_happyPath() {
        //GIVEN
        String link = "https://en.wikipedia.org/wiki/Elon_Musk";

        //WHEN
        String result = cutLink(link);

        //THEN
        Assert.assertEquals("https://en.wikipedia.org/wiki/Elon_Musk", result);

    }

    @Test
    public void testCutLink_ifContainsHash() {
        //GIVEN
        String link = "https://en.wikipedia.org/wiki/Elon_Musk#Tesla";

        //WHEN
        String result = cutLink(link);

        //THEN
        Assert.assertEquals("https://en.wikipedia.org/wiki/Elon_Musk", result);

    }
}