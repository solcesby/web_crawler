package service;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;
import java.util.Set;

public class WebCrawlerTest {

    @Test
    public void testCountMentions_happyPath() {
        //GIVEN
        WebCrawler crawler = new WebCrawler();
        String[] terms = {"что", "lorem"};

        //WHEN
        Map<String, Long> map = crawler.countMentions("https://ru.lipsum.com/", terms);

        //THEN
        Assert.assertEquals(Long.valueOf(6L), map.get(terms[0]));
        Assert.assertEquals(Long.valueOf(26L), map.get(terms[1]));
    }

    @Test
    public void testGetAllLinks_happyPath() {
        //GIVEN
        WebCrawler crawler = new WebCrawler();
        Long linkDepth = 1L;
        Long maxVisitedPages = 100L;

        //WHEN
        Set<String> links = crawler.getAllLinks("https://ru.lipsum.com/", linkDepth, maxVisitedPages);

        //THEN
        Assert.assertEquals(54L, links.size());
    }

    @Test
    public void testGetLinks() {

    }
}