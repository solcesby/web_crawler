# Web Crawler
## Information
This thing count how many times given word mentioned on the given page and other pages which linked on the given page. You can set how deep this thing will go and how many pages it will visit.

## Import Information
Some import information

## Continuous Integration Information
| Technology | Badge |
|:----------:|:-----:|
|Travis CI|[![Build Status](https://travis-ci.com/DeakerBY/Softeq_Test_Task.svg?branch=master)](https://travis-ci.com/DeakerBY/Softeq_Test_Task)|
|Codecov|[![codecov](https://codecov.io/gh/DeakerBY/Softeq_Test_Task/branch/master/graph/badge.svg)](https://codecov.io/gh/DeakerBY/Softeq_Test_Task)|

## Implementation Information
* **Programming Language:** Java
* **Build System:** [Maven](https://maven.apache.org/)
* **Automated Testing:** [JUnit4](https://junit.org/junit4/)
* **Continuous Integration:** [Travis CI](https://travis-ci.org/)
* **Code Coverage Tool:** [JaCoCo](https://www.jacoco.org/)
* **Code Coverage:** [Codecov.io](https://codecov.io/gh)
* **HTML Parser:** [Jsoup](https://jsoup.org/)

## Contacts
* **Author:** Kniga Mikhail
* **Telegram:** [@qwessdghi](https://t.me/qwessdghi)
* **Gmail:** [solcesby@gmail.com](mailto:solcesby@gmail.com)
